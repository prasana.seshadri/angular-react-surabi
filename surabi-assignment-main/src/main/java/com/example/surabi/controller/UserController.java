package com.example.surabi.controller;



import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.surabi.Temp;
import com.example.surabi.model.Admin;
import com.example.surabi.model.Menu;
import com.example.surabi.model.User;
import com.example.surabi.service.BillsService;
import com.example.surabi.service.MenuService;
import com.example.surabi.service.UserService;

@RestController
@RequestMapping("/user")

public class UserController {

	@Autowired
	UserService userService;

	@Autowired
	MenuService menuService;

	@Autowired
	BillsService billsService;

	//CREATE
	//--------------Create new User---------------------
	@PostMapping("/register")
	@CrossOrigin(origins="http://localhost:3000")
	public String create(@RequestBody User user) {

		String un=user.getUsername();
		user.setLoggedIn(0); //Since user is registering, login set to 0. 
		User u=userService.getUser(un);//Check if user already exists
		if(u!=null)
			return "User Already Exists";
		else
		{

			User temp=userService.save(user);//save new user
			return " User Added Successfully";

		}	
	}


	//UPDATE
	//--------------Update User Password from Username----------------------
	@PutMapping("/update")
	public String update(@RequestBody User user) {
		String usname=user.getUsername();
		String pwd=user.getPassword();
		User u=userService.getUser(usname);//Check if user is valid and exists.
		if(u!=null) {
			u.setPassword(pwd);
			User temp=userService.save(u);
			return "Updated";
		}
		else
			return "User does not exist";
	}




	//-------------------User Login------------------------------

	@PostMapping("/login")
	@CrossOrigin(origins="http://localhost:3000")
	public String login(@RequestBody User user) {

		Iterable<User> u=userService.findAll();//Get all users
		String usname=user.getUsername();
		String pwd=user.getPassword();
		for (User i : u)// Iterate through all users
		{
			String usname1=i.getUsername();
			String pwd2=i.getPassword();
			if(usname.equals(usname1) && pwd.equals(pwd2))
			{
				int t=i.getLoggedIn(); // Get LoggedIn Status of User

				if(t==0)
				{
					i.setLoggedIn(1); // Set LoggedIn Status to 1
					User temp=userService.save(i);
					return "Logged In successfully";
				}
				else
				{
					return "User already Logged in";
				}

			}

		}
		return "Invalid LoginId/Password";
	}

	//----------------------User Logout-------------------------------
	
	@PostMapping("/logout")
	@CrossOrigin(origins="http://localhost:3000")
	public String logout(@RequestBody User user )
	{
		

			Iterable<User> u = userService.findAll();
			String username1 = user.getUsername();

			for (User i : u) {
				String username2 = i.getUsername();

				if (username1.equals(username2)) {
					int x = i.getLoggedIn(); 

					if (x == 1) 
					{
						i.setLoggedIn(0);
						userService.save(i);
						return "User Logged Out successfully !";
					} else {
						return "User not Logged In - Kindly LOGIN";
					}

				}

			}
			return "Entered customer username and password is INVALID";
		
	}
	
	
	
	
	

	//----------------- View All Menu Items-------------------------------
	@GetMapping("/view_menu")
	@CrossOrigin(origins="http://localhost:3000")
	public Iterable<Menu> menu()
	{
		return menuService.findAll();// Return all Items of Menu
	}
	//------------------ Order Food from Menu-----------------------------
	
	@Transactional
	@PostMapping("/order")
	@CrossOrigin(origins="http://localhost:3000")
	public String order(@RequestBody String str)
	{
		Menu m;
		float amt=0;
		String items="";
		String bill="";
	
	
			
		
		JSONObject jsonObject = new JSONObject(str);
		String order = jsonObject.getString("order_items");
		
		String str1[]=order.split(",");
		
		
		int a[]=new int[str1.length];
		for(int i=0;i<str1.length;i++)
		{
			try {
			a[i]=Integer.parseInt(str1[i]);
			}
			catch(Exception e)
			{
				return "Go Back and Kindly fill in valid seq_num to place order...Eg: 1,2";
			}
		}
		for (int i:a) // for each seq_num
		{

			try {
			m=menuService.getItem(i);//Get item according to seq_num
			amt=amt+m.getPrice();
			items=items+m.getItemname();//Get Item name of seq_num
			items=items+",";		//To store bill_desc in bills table
			bill=bill+m.getItemname()+"   "+"Rs."+m.getPrice()+"<br>"; // To generate bill for user
			}
			catch (Exception e)
			{
				return "Entered seq_num did not match our menu records";
			}
			
			
		}
		items = items.substring(0, items.length() - 1); //Remove last ',' 
		
		long millis=System.currentTimeMillis();  
		Date date=new Date(millis);  //Get Date for storing in bills 
		billsService.insertIntoBill(items,amt,date); // insert new order into bills


		return "Your Order <br>"+bill+"<br>Total Amount :  Rs. "+amt;
		
	}




}
