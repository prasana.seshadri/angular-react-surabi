package com.example.surabi.controller;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.surabi.model.Admin;
import com.example.surabi.model.Bills;
import com.example.surabi.model.User;
import com.example.surabi.service.AdminService;
import com.example.surabi.service.BillsService;
import com.example.surabi.service.UserService;

@RestController
@RequestMapping("/admin")

public class AdminController {

	@Autowired
	UserService userService;

	@Autowired
	AdminService adminService;

	@Autowired
	BillsService billsService;

	//-------------------------Admin Login-------------------------------
	@CrossOrigin(origins="http://localhost:4200")
	@PostMapping("/login")
	public Admin login(@RequestBody Admin ad) {
		System.out.println("Hi");

		Iterable<Admin> admin=adminService.findAll(); //Get all admins
		String adname=ad.getAdminname();
		String pwd1=ad.getPassword();
		for ( Admin i : admin)
		{
			String adname1=i.getAdminname();
			String pwd2=i.getPassword();
			if(adname.equals(adname1) && pwd1.equals(pwd2)) // if match found
			{

				int t=i.getLoggedIn(); // Check whether logged in

//				if(t==0)
//				{
					i.setLoggedIn(1);  //Set LoggedIn status
					Admin temp=adminService.save(i);
//					System.out.println("Logged In successfully");
//					return "Logged In";
					return i;
//				}
//				else
//				{
////					System.out.println( "Admin already Logged in");
//					return i;
////					return "Already";
//				}

			}

		}
		return null;


	}
	//------------------------------Logout--------------------------------
	@CrossOrigin(origins="http://localhost:4200")
	@GetMapping("/logout/{adminid}")
	public String logout(@PathVariable int adminid )
	{
		Admin ad=adminService.getAdmin(adminid); //Get  admin with id
		System.out.println("Hi");
		
		if(ad !=null)
		{
			int t=ad.getLoggedIn();
			if(t==1)
			{
				ad.setLoggedIn(0);			//Set LoggedIn status to 0
				Admin temp=adminService.save(ad);
				return "Logged out successfully";
			}
			else
			{
				return "Admin Not Logged In....Cannot Logout";
			}
		}
		
		
		
		
		return "Invalid AdminId";
	}
	
	
	//------------------------------------------ CRUD on Users---------------------------------------------------------------------------

	//CREATE
	//--------------Create new User---------------------
	@PostMapping("/createuser")
	public String create(@RequestBody User user) {

		String un=user.getUsername();
		user.setLoggedIn(0);		//Set LoggedIn status to 0,since registering
		User u=userService.getUser(un);
		if(u!=null)
			return "User Already Exists";
		else
		{

			User temp=userService.save(user);
			return " User Added Successfully";

		}	
	}





	//	READ 
	//------------------Get All Users------------------------
	
	@GetMapping("/getallusers")
	@CrossOrigin(origins="http://localhost:3000")
	public Iterable<User> readall() {

		return userService.findAll();  //Returns all users
	}
	
	@GetMapping("/getusers")
	@CrossOrigin(origins="http://localhost:4200")
	public Iterable<User> readusers() {

		return userService.findAll();  //Returns all users
	}
	//-----------------Get Single User------------------------
	@GetMapping("/getoneuser/{userid}")
	public Optional<User> readone(@PathVariable int userid) {	

		return userService.findById(userid);	//Return Single user by id
	}


	//UPDATE
	//--------------Update User Password from Username----------------------
	@PutMapping("/updateuser")
	public String update(@RequestBody User user) {
		String usname=user.getUsername();	
		String pwd=user.getPassword();
		System.out.println(pwd);
		User u=userService.getUser(usname);
		if(u!=null) {
			u.setPassword(pwd);
			User temp=userService.save(u);
			return "Updated";
		}
		else
			return "User does not exist";
	}

	//DELETE
	//------------------Delete by userid----------------------------------
	@CrossOrigin(origins="http://localhost:4200")
	@DeleteMapping("/deleteuser/{userid}")
	public String delete(@PathVariable int userid ) {
		
		JSONObject jsonObject = new JSONObject();

		boolean u=userService.existsById(userid);
		if(u) {

			userService.deleteById(userid);
			String str="Deleted User with id  "+userid;
			
			jsonObject.put("Response",str);
			
			return jsonObject.toString();
		}
		else
		{
			String str= "User with given id does not exist";
			jsonObject.put("Response",str);
			
			return jsonObject.toString();
		}
	}


	//-----------------------------------------------------------------------------------------------------------------------------------


	//----------------View Today's bills----------------------	
	@CrossOrigin(origins="http://localhost:4200")
	@GetMapping("/view_bills_today")
	public List<Bills> dailyBills()
	{
		long millis=System.currentTimeMillis();  
		Date lt=new Date(millis); //Get Todays's Date
		return billsService.getToday(lt);
	}
	//-----------------View This Month's bills----------------
	@CrossOrigin(origins="http://localhost:4200")
	@GetMapping("/view_bills_thismonth")
	public List<Bills> monthlyBills()
	{

		return billsService.getThisMonth(); //To get current month bills
	}
	//----------------------------View Amount(Today's)------------------------
	@GetMapping("/view_bills_amount_today")
	public String dailyBillsAmount()
	{
		long millis=System.currentTimeMillis();  
		Date lt=new Date(millis);
		List<Bills> l= billsService.getToday(lt);//Get all today's bills

		float amt=0;
		for(Bills i : l)
		{
			amt=amt+i.getAmount();		//Get each bill amount and add 
		}

		return "Total sales today : "+amt;
	}
	//------------------------View Amount(Month's)----------------------------
	@CrossOrigin(origins="http://localhost:4200")
	@GetMapping("/view_bills_amount_thismonth")
	public  String monthlyBillsAmount()
	{
		JSONObject jsonObject = new JSONObject();
		

		List<Bills>l= billsService.getThisMonth(); //Get all this month's bills

		float amt=0;
		for(Bills i : l)
		{
			amt=amt+i.getAmount(); //Get each bill amount and add 
		}

		String str= "Total sales for this month :  Rs."+amt;
		jsonObject.put("Response",str);
		
		return jsonObject.toString();
	}






}
