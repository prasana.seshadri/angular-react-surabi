package com.example.surabi.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.surabi.model.Admin;
import com.example.surabi.model.User;

	
	public interface AdminService extends JpaRepository<Admin, Integer>,CrudRepository<Admin, Integer>{
		
		Admin getAdmin(int adminid);

	}


