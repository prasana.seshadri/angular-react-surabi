package com.example.surabi.service;

import java.sql.Date;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.surabi.model.Menu;
import com.example.surabi.model.User;

public interface MenuService extends JpaRepository<Menu, Integer>,CrudRepository<Menu, Integer>{

	 Menu getItem(int seq_num);
	
}
