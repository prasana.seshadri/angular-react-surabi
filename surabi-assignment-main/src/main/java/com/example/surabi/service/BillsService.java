package com.example.surabi.service;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.example.surabi.model.Bills;



 
	
	public interface BillsService extends JpaRepository<Bills, Integer>,CrudRepository<Bills, Integer>{
		
		List<Bills> getToday(Date lt);
		List<Bills> getThisMonth();
		
		@Modifying
		@Query(value="insert into bills(bill_info,amount,date)"+ "values (:bill_info,:amount,:date)",nativeQuery=true)
		public  void insertIntoBill(@Param("bill_info") String bill_info, @Param("amount") float amount,@Param("date") Date date);

	}
	
	