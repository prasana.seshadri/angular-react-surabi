package com.example.surabi.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import com.example.surabi.model.Menu;
import com.example.surabi.model.User;

public interface UserService extends JpaRepository<User, Integer>,CrudRepository<User, Integer>{

	
	
	User getUser(String username);
	User getUserid(int userid);
	
}