package com.example.surabi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurabiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SurabiApplication.class, args);
	}

}
