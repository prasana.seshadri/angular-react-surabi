package com.example.surabi.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;



@NamedNativeQuery(name="Bills.getToday",query="select * from bills where date=?1",resultClass=Bills.class)
@NamedNativeQuery(name="Bills.getThisMonth",query="select * from bills where MONTH(date)=MONTH(CURRENT_DATE())",resultClass=Bills.class)


//@Modifying
//@NamedNativeQuery(name="Bills.insertIntoBill",query="insert into bills(bill_info,amount,date) values (?1,?2,?3)",resultClass=Bills.class)




@Entity
@Table(name="bills")
public class Bills {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	String bill_info;
	float amount;
	Date date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBill_info() {
		return bill_info;
	}
	public void setBill_info(String bill_info) {
		this.bill_info = bill_info;
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(float amount) {
		this.amount = amount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
	
}
