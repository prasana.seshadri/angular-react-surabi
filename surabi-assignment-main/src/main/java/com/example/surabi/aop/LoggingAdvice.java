package com.example.surabi.aop;

import org.aspectj.lang.annotation.Around;
import com.fasterxml.jackson.datatype.jsr310.*;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

@Aspect 
@Component
public class LoggingAdvice {
	
	Logger log=LoggerFactory.getLogger(LoggingAdvice.class);
	
	@Pointcut(value="execution(* com.example.surabi.*.*.*(..))")
	public void myPointCut() {
		
	}
	
	@Around("myPointCut()")
	public Object applicationLogger(ProceedingJoinPoint pjp) throws Throwable{
		
		ObjectMapper mapper= new ObjectMapper();
		
		String methodname=pjp.getSignature().getName();
		String classname=pjp.getTarget().getClass().toString();
		Object[] arr=pjp.getArgs();
		
		log.info("-----------------------------------------AOP Logging----------------------------------------------------\n");
		log.info("Method invoked  " + classname+" : "+ methodname +"()  arguments : "+mapper.writeValueAsString(arr));
		
		Object obj=pjp.proceed();
		
		log.info(classname+" : "+ methodname +"()  Response : "+mapper.writeValueAsString(obj));
		log.info("---------------------------------------------------------------------------------------------------------\n");
		return obj;
	}

}
