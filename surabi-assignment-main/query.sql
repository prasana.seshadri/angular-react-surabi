create database surabi_restaurant;
use surabi_restaurant;


insert into admin(adminname,password,logged_in) values("surabi","Admin@123",0);
insert into users(username,password,logged_in) values("prasana","123p",0);


insert into menu values(1,"Idli",35);
insert into menu values(2,"Dosa",55);
insert into menu values(3,"Pongal",45);
insert into menu values(4,"Poori",40);
insert into menu values(5,"Chapathi(2 Nos)",30);
insert into menu values(6,"Poha",25);
insert into menu values(7,"Aloo Paratha",50);
insert into menu values(8,"Roti",15);
insert into menu values(9,"Naan",30);
insert into menu values(10,"Kulcha",55);
insert into menu values(11,"Paratha(Plain)",35);
insert into menu values(12,"Gobi gravy",75);
insert into menu values(13,"Kadai Paneer",80);
insert into menu values(14,"Veg Kuruma",60);
insert into menu values(15,"Dum Aloo Masala",85);
insert into menu values(16,"Veg Biryani",75);
insert into menu values(17,"Veg Fried Rice",80);
insert into menu values(18,"Jeera Rice",65);
insert into menu values(19,"Curd Rice",50);
insert into menu values(20,"Ice Cream(cup)",30);
insert into menu values(21,"Ice Cream(cone)",35);
insert into menu values(22,"Chocolate Milkshake",60);
insert into menu values(23,"Vanilla Milkshake",60);
insert into menu values(24,"Gulab Jamun(2 Nos)",40);
insert into menu values(25,"Jilebi(100g)",30);

