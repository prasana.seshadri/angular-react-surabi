import { Component, OnInit } from '@angular/core';
import { Order } from '../order';
import { VieworderService } from '../vieworder.service';

@Component({
  selector: 'app-vieworder',
  templateUrl: './vieworder.component.html',
  styleUrls: ['./vieworder.component.css']
})
export class VieworderComponent implements OnInit {

  constructor(private _service:VieworderService) { }

  noorder:boolean=false
  orders:Order[]=new Array<Order>();

  ngOnInit(): void {

    this._service.getOrders().subscribe(
      
      
      data=>{
        console.log(data)
        var count=Object.keys(data).length;
        console.log(count)
        if(count===0)
        {
            this.noorder=true
        }
        else
          this.orders=data
      }
    )

  }


}
