import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ViewuserService {

  constructor(private _http:HttpClient) { }

  public getUsers():Observable<any>{

    return this._http.get("http://localhost:8080/admin/getusers")
  }
}
