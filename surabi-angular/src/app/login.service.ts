import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Admin } from './admin';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private _http:HttpClient) { }

  public loginAdmin(admin:Admin):Observable<any>{
    return this._http.post("http://localhost:8080/admin/login",admin)
  }
}
