import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VieworderService {

  constructor(private _http:HttpClient) { }

  public getOrders():Observable<any>{

    return this._http.get("http://localhost:8080/admin/view_bills_today")
  }
}
