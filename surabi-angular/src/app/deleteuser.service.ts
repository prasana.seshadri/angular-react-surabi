import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeleteuserService {

  constructor(private _http:HttpClient) { }

  public deleteUser(deletefield:Number):Observable<any>{
    console.log(deletefield)
    return this._http.delete(`http://localhost:8080/admin/deleteuser/${deletefield}`)
  }
}
