import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LogoutService } from '../logout.service';
import { Admin } from '../admin';
import { MonthlysalesService } from '../monthlysales.service';
import { DeleteuserService } from '../deleteuser.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private _service:LogoutService,private ser:MonthlysalesService,private delser:DeleteuserService ,private _router:Router) { }
  admin = new Admin();

  comment:string='';
  commentdisp:boolean=false;

  deletemsg:string='';
  deletemsgdisp:boolean=false;

  deletefield:string='';
  txtdisp:boolean=false;

  del:Number=NaN
  

  ngOnInit(): void {
  }

  viewOrders()
  {
    this._router.navigate(['/vieworder'])

  }
  logout()
  {
    
    this._service.logoutAdmin(this.admin).subscribe(
      data=>console.log(data),
      error=>console.log(error)
      
    )
    this._router.navigate([''])
  }

  viewAmount()
  {
    console.log("amt")
    this.txtdisp=false
    this.deletemsgdisp=false
    this.ser.getAmount().subscribe(
      data=>{console.log(data.Response)
        this.commentdisp=true
      this.comment=data.Response}
      )
      
    
  }
  viewUsers()
  {
    this._router.navigate(['/viewusers'])
  }

  deleteUser1()
  {
    console.log("delete")
    this.txtdisp=true;
    this.commentdisp=false
  }

  deleteUser()
  {
    
    
    this.del=Number(this.deletefield)
    this.delser.deleteUser(this.del).subscribe(
      data=>{
        this.deletemsgdisp=true
        this.commentdisp=false
        console.log(data.Response);
        this.deletemsg=data.Response
      },
      error=>
      {
        this.deletemsgdisp=true
        this.commentdisp=false
        console.log("error")
        this.deletemsg="Enter valid id"
      }

    )
  }

}
