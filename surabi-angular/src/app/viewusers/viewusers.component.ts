import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { ViewuserService } from '../viewuser.service';

@Component({
  selector: 'app-viewusers',
  templateUrl: './viewusers.component.html',
  styleUrls: ['./viewusers.component.css']
})
export class ViewusersComponent implements OnInit {

  constructor(private _service:ViewuserService) { }
  users:User[]=new Array<User>();
  nouser:boolean=false
  ngOnInit(): void {

    this._service.getUsers().subscribe(
      data=>
      {
        console.log(data)
        var count=Object.keys(data).length;
        console.log(count)
        if(count===0)
        {
            this.nouser=true
        }
        else
          this.users=data
      }
      

    )

}
}
