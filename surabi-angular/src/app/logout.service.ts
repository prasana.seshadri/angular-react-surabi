import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Admin } from './admin';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  constructor(private _http:HttpClient) { }

  public logoutAdmin(admin:Admin):Observable<any>{
    
    admin.adminid=Number(localStorage.getItem("adminid"));
    console.log(admin.adminid)
    localStorage.clear();
    return this._http.get(`http://localhost:8080/admin/logout/${admin.adminid}`)

    
  }
}
