import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { LoginComponent } from './login/login.component';
import { VieworderComponent } from './vieworder/vieworder.component';
import { ViewusersComponent } from './viewusers/viewusers.component';


const routes: Routes = [
  {path:"",component:LoginComponent},
  {path:"homepage",component:HomepageComponent},
  {path:"vieworder",component:VieworderComponent},
  {path:"viewusers",component:ViewusersComponent}
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
