
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Admin } from '../admin';
import { LoginService } from '../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  admin = new Admin()
  error:boolean=false
  

  constructor(private _service:LoginService,private _router:Router) { }

  ngOnInit(): void {
  }

  checkLogin()
  {
    this._service.loginAdmin(this.admin).subscribe(
      data=> {
        console.log("Response received")
        if(data==null)
          this.error=true
        else
        {
          this.error=false
          localStorage.setItem('adminid',data.adminid.toString())
          localStorage.setItem('adminname',data.adminname.toString())
          this._router.navigate(['/homepage']);
          console.log(data)
        }
    },
      error=> console.log("error")
    )
  }
}
