import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MonthlysalesService {

  constructor(private _http:HttpClient) { }

 public getAmount():Observable<any>{

  return this._http.get("http://localhost:8080/admin/view_bills_amount_thismonth")
 }

}
