import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Form } from 'react-bootstrap';
import { useHistory,useLocation } from "react-router-dom";
import hdr_img from '/Users/prasana/Desktop/CSE-565(SVVT)/Assignment-5/angular-react-surabi/react/react-surabi/src/components/logo.jpg'

const Menu = (props) => {
  

  const [items, setitems] = useState(null);
  const[isAlert,setAlert]=useState(false);
  const [order, setOrder] = useState('');

  useEffect(() => {
    getData();

    async function getData() {
      const response = await fetch("http://localhost:8080/user/view_menu");
      const data = await response.json();

      setitems(data);
    }
  }, []);

  

  const history = useHistory();
  const location = useLocation();


  const us=location.state.propusername;
  const pass=location.state.proppassword;

  const onSubmitSignIn = () => {
    
    fetch('http://localhost:8080/user/logout', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: us,
        password: pass
      })
    })
    fetch('http://localhost:8080/admin/getallusers')
      .then(response => response.json())
      .then(data => {
        data.forEach(function (user) {
          if (us=== user.username && pass === user.password) {
            console.log("login done");
            history.push("/sign-in");
          }
        });
      });

  }



  
  
  const onArrayChange = (event) => {

    setOrder(event.target.value);
  }



  async function onOrder  (event) {

    // setAlert(true);
     event.preventDefault();
  
    let response= await fetch('http://localhost:8080/user/order', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
       order_items : order
      })
    });

    let res=await response.text();
    console.log(res);
    history.push({
      pathname: '/payment',
      state: {
        props_bill:res,
        propusername:us,
        proppassword:pass
      }
    });
    

  
  }



  return (

    <div>
  <br></br>
   
    <h1><img src={hdr_img} width={150} height={150}/>Surabi Restaurant</h1>
    <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
      <Button as="input" type="submit" onClick={onSubmitSignIn} value="Logout"/>
      </div>
      
     
      
      {items && (
        <div className="items">
          <br></br>
    <Form.Control type="text" onChange={onArrayChange} placeholder="Enter the sequence numbers of the items seperated by ,(comma)....Eg : 1,2" />
    <br></br>
    { isAlert  && ( <div id="al" class="alert alert-warning" role="alert"></div>)}
        <center>
    <Button as="input" type="submit" onClick={onOrder} value="Place Order" />
    </center>
    <br></br>
          <h1 className = "text-center">Menu</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>
                            <th>Seq Num</th>
                            <th>Item Name</th>
                            <th>Price</th>
                            
                        </tr>
                    </thead>

                    <tbody>
                        {
                            items.map(
                                item =>
                                <tr key = {item.seq_num}>
                                    <td>{item.seq_num}</td>
                                    <td>{item.itemname}</td>
                                    <td>{item.price}</td>
                                    
                                </tr>
                            )
                            }
                    </tbody>
                </table>
          <Form>
           <Form.Group className="mb-3" controlId="formBasicEmail">
             <br></br>
    
    
  </Form.Group>
  </Form>



        </div>
      )}
    </div>


  );
}
export default Menu;



