import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import "./Login.css";



const Login = (props) => {

  const [userName, setUserName] = useState('');
  const [userPassword, setPassword] = useState('');
  const [isAlert, setAlert] = useState(false);
  const [checkbox_checked, setCheckboxChecked]=useState(false);
  const history = useHistory();

  function validateForm() {
    return userName.length > 0 && userPassword.length > 0 && checkbox_checked;
  }

  const userchange = (e) => {
    setUserName(e.target.value)
    setAlert(false)
  }


  const userTypechange = (e) => {
    console.log(e.target.value)
    if(e.target.value==="New")
    history.push({
      pathname: '/sign-up',
    });
  }

  const handleChange = event => {
    if (event.target.checked) {
      console.log('True')
      setCheckboxChecked(true);
    } else {
      console.log('False')
      setCheckboxChecked(false);
    }

  };

  const pwdchange = (e) => {
    setPassword(e.target.value)
    setAlert(false)
  }
  const onSubmitSignIn = (event) => {
    let flag=0;
    let a;
    let b;
    event.preventDefault();
    fetch('http://localhost:8080/user/login', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username: userName,
        password: userPassword
      })
    })
    fetch('http://localhost:8080/admin/getallusers')
      .then(response => response.json())
      .then(data => {
        data.forEach(function (user) {
          if (userName === user.username && userPassword === user.password) 
          {
            console.log(user)
            if (user.loggedIn === 1) {
              a = userName;
              b = userPassword;

              flag=1;
            }
            else if (user.loggedIn === 0) {
              console.log("login done");
              a = userName;
              b = userPassword;

              flag=2;
            }
            
          }
        });

        if (flag===0) {
          setAlert(true);
          document.getElementById("al").innerHTML = "Invalid Login Credentials";
        }
        else if (flag===2) {
          history.push({
            pathname: '/menu',
            state: {
              proppassword: b,
              propusername: a

            }
          });
        }
        else if (flag===1) {
         
          alert("Already Logged In..Click Ok to view Menu")

          history.push({
            pathname: '/menu',
            state: {
              proppassword: b,
              propusername: a

            }
          });
        }
      });

  }

  return (





    <div className="Login">
    
      <center><h3>User Login</h3></center>
      <input id='current' onChange={userTypechange} type="radio" value="Existing" name="userType" checked="checked"/> Existing User &nbsp;
      <input id='new' onChange={userTypechange} type="radio" value="New" name="userType" /> New User
     
      <Form>
        <Form.Group size="lg" controlId="email">
          <b><Form.Label>Username</Form.Label></b>
          <Form.Control
            autoFocus
            id='email_field'
            type="email"
            value={userName}
            onChange={userchange}
            placeholder="User Name"
          />
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <b><Form.Label>Password</Form.Label></b>
          <Form.Control
            type="password"
            id='pwd_field'
            value={userPassword}
            onChange={pwdchange}
            placeholder="Password"
          />
        </Form.Group>
      </Form>

      <br></br>
      <input id='robot_check' onChange={handleChange} type='checkbox' /> I'm not a robot.

      <center>
      <br></br>
        <Button id='login-btn' type="submit" onClick={onSubmitSignIn} disabled={!validateForm()}>
          Login
        </Button>
        <br></br>
         {/* <Link to="/sign-up">
          New User ? Register
        </Link> */}
        {isAlert && (<div id="al" class="alert alert-warning" role="alert"></div>)}
      </center>


    </div>
  );
}
export default Login;


