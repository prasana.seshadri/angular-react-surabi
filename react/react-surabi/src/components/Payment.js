import React, { useState, useEffect } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Button,Form } from 'react-bootstrap';
import { useHistory,useLocation } from "react-router-dom";
import logo from '/Users/prasana/Desktop/CSE-565(SVVT)/Assignment-5/angular-react-surabi/react/react-surabi/src/components/cooking-min.gif'
const Payment=(props)=>{
    const location = useLocation();
    const res=location.state.props_bill;
    const us=location.state.propusername;
    const pass=location.state.proppassword;
    const[isAlert,setAlert]=useState(false);
    const[hide,setHide]=useState(true);
    const [payment_mode,setPaymentMode]=useState('');
    useEffect(() => {
            document.getElementById('al').innerHTML=res;
        },[]);

const handlePayment = (e) => {
            setHide(false);
            if(e.target.value=="Card"){
                setPaymentMode("Card");
            }
        else{
            setPaymentMode("Cash");
          }
        }
const handlePay = (e) => {

    fetch('http://localhost:8080/user/logout', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          username: us,
          password: pass
        })
      })
            let element = document.getElementById("paid");
            element.setAttribute("hidden", "hidden");
            const text1="Payment Succesful. Payment option Selected:";
            console.log(payment_mode)
            const text=text1.concat('',payment_mode);
            document.getElementById('al').innerHTML=text;
            
          }

    return (
        <body>
            <center>
            <h1>Thanks for Ordering!</h1>
            <img src={logo} alt="Cooking..." width={500} height={500}/>

            <div id="al" class="alert alert-warning" role="alert"></div>
            <div id="paid">
            <Form.Control style={{width: 500}} type="text" placeholder="Cooking Instructions (if any)" />
            <br></br>
            <label>Payment Mode</label>
            <br></br>
            <br></br>
            <input  onChange={handlePayment} type="radio" value="Cash" name="payment"/> Cash &nbsp;
            <input  onChange={handlePayment} type="radio" value="Card" name="payment"/> Card
            <br></br>
            <br></br>
            <Button type="submit" onClick={handlePay} disabled={hide}>
                Pay
            </Button>
            </div>
            </center>
        <br></br>
        </body>
    );
}
export default Payment