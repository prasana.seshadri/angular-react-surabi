import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";


const Register = (props) => {

  const [userName, setName] = useState('');
  const [userPassword, setPassword] = useState('');
  const [isAlert, setAlert] = useState(false);
  const [checkbox_checked, setCheckboxChecked]=useState(false);

  const onNameChange = (event) => {
    setAlert(false)
    setName(event.target.value);
  }

  const onPasswordChange = (event) => {
    setAlert(false)
    setPassword(event.target.value);
  }

  const handleChange = event => {
    if (event.target.checked) {
      console.log('True')
      setCheckboxChecked(true);
    } else {
      console.log('False')
      setCheckboxChecked(false);
    }

  };
  function validateForm() {
    return userName.length > 0 && userPassword.length > 0 && checkbox_checked;
  }


  const history = useHistory();
  const onSubmitSignIn = () => {
    let flag = 0;
    fetch('http://localhost:8080/admin/getallusers')
      .then(response => response.json())
      .then(data => {
        data.forEach(function (user) {
          if (userName === user.username) {
            setAlert(true);
            document.getElementById("al").innerHTML = "UserName Already Exists...Try another one";

            flag = 1;
          }
        });
        if (flag === 0) {
          flag = 1;
          fetch('http://localhost:8080/user/register', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
              username: userName,
              password: userPassword
            })
          })
          history.push("/sign-in");
        }
      });

  }




  return (

    <div className="Login">
      <center><h3>Register</h3></center>
      <Form>
        <Form.Group size="lg" controlId="email">
          <b><Form.Label>Username</Form.Label></b>
          <Form.Control
            autoFocus
            id='email_fld'
            type="email"
            value={userName}
            onChange={onNameChange}
            placeholder="User Name"
          />
        </Form.Group>
        <Form.Group size="lg" controlId="password">
          <b><Form.Label>Password</Form.Label></b>
          <Form.Control
            type="password"
            id='pwd_fld'
            value={userPassword}
            onChange={onPasswordChange}
            placeholder="Password"
          />
        </Form.Group>
      </Form>
      <br></br>
      
      <input id='check_robot' onChange={handleChange} type='checkbox' /> I'm not a robot.

      <br></br>
      <center>
      <Button id='register-btn'type="submit" onClick={onSubmitSignIn} disabled={!validateForm()}>
          Register
        </Button>
      <br></br>
       
        <br></br>
        {/* <Link to="/sign-in">
          Login
        </Link> */}

        <br></br>
        {isAlert && (<div id="al" class="alert alert-warning" role="alert"></div>)}
      </center>

    </div>

  );
}
export default Register;