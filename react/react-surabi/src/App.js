import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
// import "./App.css";
import Menu from './components/Menu';
import Login from "./components/Login";
import SignUp from "./components/Register";
import Payment from './components/Payment';

function App() {
  return (<Router>
    <div className="App">
      

      <div className="auth-wrapper">
        <div className="auth-inner">
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path="/sign-in" component={Login} />
            <Route path="/menu" component={Menu} />
            <Route path="/sign-up" component={SignUp} />
            <Route path="/payment" component={Payment} />
          </Switch>
        </div>
      </div>
    </div></Router>
  );
}

export default App;
