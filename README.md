**Folder Structure**
1) react/react-surabi -  Front-end react
2) surabi-assignment-main -Backend Springboot
3) surabi-angular - Front-end Angular

**Steps**
1) create database surabi_restaurant
2) use surabi_restaurant
3) import surabi-assignment-main folder as existing maven project in IDE.
4) Change username, password in application properties file.
6) start the application SurabiApplication in surabi-assignment-main folder and run on localhost:8080
7) Check if tables are created. Then, Insert admin, user and menu into the database using source command.(query.sql attached)
6) open react in code editor
7) Change directory to react-surabi
8) npm install
9) npm start
10) New tab opens in browser, else run on localhost:3000
11) open surabi-angular in code editor
12) npm install
13) ng serve --open

**SQL file has been added inside surabi-assignment-main folder**

